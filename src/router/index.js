import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Refactor from '@/components/Refactor'
import ErrorHandling from '@/components/ErrorHandling'
import TransitionAnimation from '@/components/TransitionAnimation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/refactor',
      name: 'Refactor',
      component: Refactor
    },
    {
      path: '/error-handling',
      name: 'Field-level error handling',
      component: ErrorHandling
    },
    {
      path: '/transition-animation',
      name: 'Transition animation',
      component: TransitionAnimation
    }
  ]
})
